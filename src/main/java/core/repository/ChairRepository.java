package core.repository;

import core.model.CmsException;
import core.model.Chair;
import core.model.Participant;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 13.05.2017.
 */
public class ChairRepository {
    private Connection connection;
    private ParticipantRepository participantRepository;

    public ChairRepository(Connection connection, ParticipantRepository participantRepository) throws SQLException {
        this.connection = connection;
        this.participantRepository = participantRepository;
    }

    public Chair findOne(String username) throws SQLException {
        String selectStatement = "SELECT username FROM chairs WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(selectStatement);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            throw new CmsException("There is no chair with such a username!");
        }
        return new Chair(participantRepository.findOne(username));
    }

    public boolean exists(String username) throws SQLException {
        String selectStatement = "SELECT username FROM chairs WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(selectStatement);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next();
    }

    public Chair save(String username) throws SQLException {
        if (exists(username)) {
            throw new CmsException("Tried to add duplicate username: " + username);
        }

        if (!participantRepository.exists(username)) {
            throw new CmsException("No such username " + username);
        }

        String insertStatement = "INSERT INTO chairs (username) VALUES (?)";
        PreparedStatement preparedStatement = connection.prepareStatement(insertStatement);
        preparedStatement.setString(1, username);
        preparedStatement.executeUpdate();
        return new Chair(participantRepository.findOne(username));
    }

    public Chair update(Chair chair) throws SQLException {
        if (!exists(chair.getUsername())) {
            throw new CmsException("Tried to update a non existing chair!");
        }

        return new Chair(participantRepository.update(chair));
    }

    public void delete(String username) throws SQLException {
        if (!exists(username)) {
            throw new CmsException("Tried to delete non existing entity!");
        }

        String deleteQuery = "DELETE FROM chairs WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(deleteQuery);
        statement.setString(1, username);
        statement.executeUpdate();
    }

    public List<Chair> findAll() throws SQLException {
        String selectStatement = "SELECT username FROM chairs";
        PreparedStatement statement = connection.prepareStatement(selectStatement);
        ResultSet resultSet = statement.executeQuery();
        List<Chair> chairs = new ArrayList<>();
        while (resultSet.next()) {
            String username = resultSet.getString("username");
            chairs.add(new Chair(participantRepository.findOne(username)));
        }
        return chairs;
    }
}
