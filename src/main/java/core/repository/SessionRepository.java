package core.repository;

import core.model.CmsException;
import core.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diana on 13-May-17.
 */
public class SessionRepository implements Repository<Integer,Session> {

    private Connection connection;

    public SessionRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean exists(Integer id) throws SQLException {
        String selectQuery = "SELECT * FROM session " +
                "WHERE session_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet.next();
    }

    @Override
    public Session findOne(Integer id) throws SQLException {
        String selectQuery = "SELECT * FROM session W" +
                "HERE session_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(!resultSet.next()) {
            throw new CmsException("Tried to get invalid session! No such id: " + id);
        }
        return new Session(resultSet);
    }

    @Override
    public Session save(Session session) throws SQLException {
        String insertQuery = "INSERT INTO SESSION (type, conferenceId, chairId) " +
                "VALUES (?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(insertQuery);
        statement.setString(1, session.getType());
        statement.setInt(2, session.getConferenceId());
        statement.setInt(3, session.getChairId());
        statement.executeUpdate();

        return session;
    }

    @Override
    public void delete(Integer id) throws SQLException {
        if (!exists(id)) {
            throw new CmsException("Tried to delete a session which was not in the current database!");
        }

        String deleteQuery = "DELETE FROM SESSION WHERE session_id = ?";
        PreparedStatement statement = connection.prepareStatement(deleteQuery);
        statement.setInt(1, id);
        statement.executeUpdate();
    }

    @Override
    public Session update(Session session) throws SQLException {
        if (!exists(session.getSession_id())) {
            throw new CmsException("Tried to update a session which is not in the current database!");
        }
        //TODO validate
        PreparedStatement statement = connection.prepareStatement("UPDATE session " +
                "SET type = ?, conferenceId = ?, chiarId = ?" +
                "WHERE session_id = ?");
        statement.setString(1, session.getType());
        statement.setInt(2, session.getConferenceId());
        statement.setInt(3, session.getChairId());
        statement.setInt(4, session.getSession_id());
        statement.executeUpdate();
        return session;
    }

    @Override
    public List<Session> findAll() throws SQLException {
        List<Session> allSessions = new ArrayList<>();
        String selectQuery = "SELECT * FROM session";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            allSessions.add(new Session(resultSet));
        }
        return allSessions;
    }
}
