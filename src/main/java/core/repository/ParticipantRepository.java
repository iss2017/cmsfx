package core.repository;


import core.model.CmsException;
import core.model.Participant;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 13.05.2017.
 */
public class ParticipantRepository implements Repository<String, Participant> {
    private Connection connection;

    public ParticipantRepository(Connection connection) throws SQLException {
        this.connection = connection;
    }

    @Override
    public Participant save(Participant participant) throws SQLException {
        //TODO validate entries
        if (exists(participant.getUsername())) {
            throw new CmsException("Tried to add a participant which has an already used username!");
        }
        String insertQuery = "INSERT INTO participants (username, password, name, affiliation, " +
                "email, web_page) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(insertQuery);
        statement.setString(1, participant.getUsername());
        statement.setString(2, participant.getPassword());
        statement.setString(3, participant.getName());
        statement.setString(4, participant.getAffiliation());
        statement.setString(5, participant.getEmail());
        statement.setString(6, participant.getWebPage());
        statement.executeUpdate();

        return participant;
    }

    @Override
    public boolean exists(String username) throws SQLException {
        String selectQuery = "SELECT * FROM participants WHERE username = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, username);
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet.next();
    }

    @Override
    public Participant findOne(String username) throws SQLException {
        String selectQuery = "SELECT * FROM participants WHERE username = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, username);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            throw new CmsException(username + " is not in the current database!");
        }
        return new Participant(resultSet);
    }

    @Override
    public List<Participant> findAll() throws SQLException {
        List<Participant> allParticipants = new ArrayList<>();
        String selectQuery = "SELECT * FROM participants";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            allParticipants.add(new Participant(resultSet));
        }
        return allParticipants;
    }

    @Override
    public void delete(String username) throws SQLException {
        if (!exists(username)) {
            throw new CmsException("Tried to delete a username which was not in the current database!");
        }

        String deleteQuery = "DELETE FROM participants WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(deleteQuery);
        statement.setString(1, username);
        statement.executeUpdate();
    }

    @Override
    public Participant update(Participant entity) throws SQLException {
        if (!exists(entity.getUsername())) {
            throw new CmsException("Tried to update a participant which is not in the current database!");
        }
        //TODO validate
        PreparedStatement statement = connection.prepareStatement("UPDATE participants " +
                "SET password = ?, name = ?, affiliation = ?, email = ?, web_page = ?" +
                "WHERE username = ?");
        statement.setString(1, entity.getPassword());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getAffiliation());
        statement.setString(4,entity.getEmail());
        statement.setString(5, entity.getWebPage());
        statement.setString(6, entity.getUsername());
        statement.executeUpdate();
        return entity;
    }
}
