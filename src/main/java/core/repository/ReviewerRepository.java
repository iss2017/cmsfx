package core.repository;

import core.model.CmsException;
import core.model.Listener;
import core.model.Reviewer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rosi on 13.05.2017.
 */
public class ReviewerRepository {
    private Connection connection;
    private ParticipantRepository participantRepository;

    public ReviewerRepository(Connection connection, ParticipantRepository participantRepository) throws SQLException {
        this.connection = connection;
        this.participantRepository = participantRepository;
    }

    public boolean exists(String username) throws SQLException {
        String selectQuery = "SELECT * FROM reviewers WHERE username = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, username);
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet.next();
    }

    public Reviewer findOne(String username) throws SQLException {
        String selectQuery = "SELECT * FROM reviewers WHERE username = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, username);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            throw new CmsException(username + " is not in the current database as a reviewer!");
        }
        return new Reviewer(participantRepository.findOne(username));
    }

    public Reviewer save(String username) throws SQLException {
        //TODO validate entries
        if (exists(username)) {
            throw new CmsException("Reviewer already exists!");
        }

        if (!participantRepository.exists(username)) {
            throw new CmsException("A participant with such a username does not exist!");
        }

        String insertQuery = "INSERT INTO reviewers (username) VALUES (?)";
        PreparedStatement statement = connection.prepareStatement(insertQuery);
        statement.setString(1, username);

        statement.executeUpdate();

        return new Reviewer(participantRepository.findOne(username));
    }

    public void delete(String username) throws SQLException {
        if (!exists(username)) {
            throw new CmsException("Tried to delete a username which was not in the current database as a reviewer!");
        }

        String deleteQuery = "DELETE FROM reviewers WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(deleteQuery);
        statement.setString(1, username);
        statement.executeUpdate();
    }

    public Reviewer update(Reviewer entity) throws SQLException {
        if (!exists(entity.getUsername())) {
            throw new CmsException("Tried to update a non existing reviewer!");
        }

        return new Reviewer(participantRepository.update(entity));
    }

    public List<Reviewer> findAll() throws SQLException {
        String selectStatement = "SELECT username FROM reviewers";
        PreparedStatement statement = connection.prepareStatement(selectStatement);
        ResultSet resultSet = statement.executeQuery();
        List<Reviewer> reviewers = new ArrayList<>();
        while (resultSet.next()) {
            String username = resultSet.getString("username");
            reviewers.add(new Reviewer(participantRepository.findOne(username)));
        }
        return reviewers;
    }
}
