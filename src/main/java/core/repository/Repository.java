package core.repository;

import java.sql.SQLException;
import java.util.List;

/**
 * Created on 13.05.2017.
 */
public interface Repository<ID, T> {
    boolean exists(ID id) throws SQLException;
    T findOne(ID id) throws SQLException;
    T save(T entity) throws SQLException;
    void delete(ID id) throws SQLException;
    T update(T entity) throws SQLException;
    List<T> findAll() throws SQLException;
}
