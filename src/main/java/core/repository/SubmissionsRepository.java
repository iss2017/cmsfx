package core.repository;


import core.model.CmsException;
import core.model.Submission;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 13.05.2017.
 */
public class SubmissionsRepository implements Repository<Integer, Submission> {
    private Connection connection;

    public SubmissionsRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean exists(Integer id) throws SQLException {
        String selectQuery = "SELECT title FROM submissions W" +
                "HERE submission_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet.next();
    }

    @Override
    public Submission findOne(Integer id) throws SQLException {
        String selectQuery = "SELECT * FROM submissions WHERE submission_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(!resultSet.next()) {
            throw new CmsException("Tried to get invalid submission! No such id: " + id);
        }
        return new Submission(resultSet);
    }

    @Override
    public Submission save(Submission entity) throws SQLException {
        String insertQuery = "INSERT INTO submissions (title, session_id, keywords, abstract_name, abstract_file, " +
                "paper_name, paper_file) VALUES (?, ?, ?, ?, ?, ?, ?) RETURNING submission_id;";

        PreparedStatement statement = connection.prepareStatement(insertQuery);
        statement.setString(1, entity.getTitle());
        if (entity.getSessionId() != null) {
            statement.setInt(2, entity.getSessionId());
        } else {
            statement.setNull(2, Types.INTEGER);
        }
        statement.setString(3, entity.getKeywords());
        statement.setString(4, entity.getAbstractName());
        statement.setBytes(5, entity.getAbstractFile());
        statement.setString(6, entity.getPaperName());
        statement.setBytes(7, entity.getPaperFile());
        ResultSet set = statement.executeQuery();

        if (set.next()) {
            entity.setSubmissionId(set.getInt("submission_id"));
        }

        return entity;
    }

    @Override
    public void delete(Integer id) throws SQLException {
        if (!exists(id)) {
            throw new CmsException("Tried to delete non-existing submission!");
        }

        String deleteQuery = "DELETE FROM submissions WHERE submission_id = ?";
        PreparedStatement statement = connection.prepareStatement(deleteQuery);
        statement.setInt(1, id);
        statement.executeUpdate();
    }

    @Override
    public Submission update(Submission entity) throws SQLException {
        if (!exists(entity.getSubmissionId())) {
            throw new CmsException("Tried to update a submission which is not in the current database!");
        }
        //TODO validate
        PreparedStatement statement = connection.prepareStatement("UPDATE submissions " +
                "SET title = ?, keywords = ?, session_id = ?" +
                "WHERE submission_id = ?");
        statement.setString(1, entity.getTitle());
        statement.setString(2, entity.getKeywords());
        statement.setInt(3, entity.getSessionId());
        statement.setInt(4, entity.getSubmissionId());
        statement.executeUpdate();

        return entity;
    }

    @Override
    public List<Submission> findAll() throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT submission_id, title, session_id, " +
                " keywords, abstract_name, paper_name FROM submissions");

        List<Submission> submissions = new ArrayList<>();
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            submissions.add(new Submission(resultSet));
        }
        return submissions;
    }
}
