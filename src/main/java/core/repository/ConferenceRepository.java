package core.repository;

import core.model.CmsException;
import core.model.Conference;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Diana on 13-May-17.
 */
public class ConferenceRepository implements Repository<Integer,Conference> {

    private Connection connection;

    public ConferenceRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean exists(Integer id) throws SQLException {
        String selectQuery = "SELECT * FROM conference " +
                "WHERE conference_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet.next();
    }

    @Override
    public Conference findOne(Integer id) throws SQLException {
        String selectQuery = "SELECT * FROM conference W" +
                "HERE conference_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(!resultSet.next()) {
            throw new CmsException("Tried to get invalid session! No such id: " + id);
        }
        return new Conference(resultSet);
    }

    private String getStringFromDate(Date date) {
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(date);
        return currentTime;
    }

    @Override
    public Conference save(Conference conference) throws SQLException {
        String insertQuery = "INSERT INTO SESSION (name, startDate, endDate, deadline) " +
                "VALUES (?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(insertQuery);
        statement.setString(1, conference.getName());
        String startDate = getStringFromDate(conference.getStartDate());
        String endDate = getStringFromDate(conference.getEndDate());
        String deadline = getStringFromDate(conference.getDeadline());
        statement.setString(2, startDate);
        statement.setString(3, endDate);
        statement.setString(4, deadline);
        statement.executeUpdate();
        return conference;
    }

    @Override
    public void delete(Integer id) throws SQLException {
        if (!exists(id)) {
            throw new CmsException("Tried to delete a conference which was not in the current database!");
        }

        String deleteQuery = "DELETE FROM conference WHERE conference_id = ?";
        PreparedStatement statement = connection.prepareStatement(deleteQuery);
        statement.setInt(1, id);
        statement.executeUpdate();
    }

    @Override
    public Conference update(Conference conference) throws SQLException {
        if (!exists(conference.getConference_id())) {
            throw new CmsException("Tried to update a conference which is not in the current database!");
        }
        //TODO validate
        PreparedStatement statement = connection.prepareStatement("UPDATE conference " +
                "SET name = ?, startDate = ?, endDate = ?, deadline = ?" +
                "WHERE conference_id = ?");
        statement.setString(1, conference.getName());
        String startDate = getStringFromDate(conference.getStartDate());
        String endDate = getStringFromDate(conference.getEndDate());
        String deadline = getStringFromDate(conference.getDeadline());
        statement.setString(2, startDate);
        statement.setString(3, endDate);
        statement.setString(4, deadline);
        statement.setInt(5, conference.getConference_id());
        statement.executeUpdate();
        return conference;
    }

    @Override
    public List<Conference> findAll() throws SQLException {
        List<Conference> allConferences = new ArrayList<>();
        String selectQuery = "SELECT * FROM conference";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            allConferences.add(new Conference(resultSet));
        }
        return allConferences;
    }
}
