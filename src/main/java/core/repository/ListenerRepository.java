package core.repository;

import core.model.CmsException;
import core.model.Listener;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 13.05.2017.
 */
public class ListenerRepository {
    private Connection connection;
    private ParticipantRepository participantRepository;

    public ListenerRepository(Connection connection, ParticipantRepository participantRepository) throws SQLException {
        this.connection = connection;
        this.participantRepository = participantRepository;
    }

    public Listener findOne(String username) throws SQLException {
        String selectStatement = "SELECT username FROM listeners WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(selectStatement);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            throw new CmsException("There is no listener with such a username!");
        }
        return new Listener(participantRepository.findOne(username));
    }

    public boolean exists(String username) throws SQLException {
        String selectStatement = "SELECT username FROM listeners WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(selectStatement);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next();
    }

    public Listener save(String username) throws SQLException {
        if (exists(username)) {
            throw new CmsException("Tried to add duplicate username: " + username);
        }

        if (!participantRepository.exists(username)) {
            throw new CmsException("No such username " + username);
        }

        String insertStatement = "INSERT INTO listeners (username) VALUES (?)";
        PreparedStatement preparedStatement = connection.prepareStatement(insertStatement);
        preparedStatement.setString(1, username);
        preparedStatement.executeUpdate();
        return new Listener(participantRepository.findOne(username));
    }

    public Listener update(Listener listener) throws SQLException {
        if (!exists(listener.getUsername())) {
            throw new CmsException("Tried to update a non existing listener!");
        }

        return new Listener(participantRepository.update(listener));
    }

    public void delete(String username) throws SQLException {
        if (!exists(username)) {
            throw new CmsException("Tried to delete non existing entity!");
        }

        String deleteQuery = "DELETE FROM listeners WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(deleteQuery);
        statement.setString(1, username);
        statement.executeUpdate();
    }

    public List<Listener> findAll() throws SQLException {
        String selectStatement = "SELECT username FROM listeners";
        PreparedStatement statement = connection.prepareStatement(selectStatement);
        ResultSet resultSet = statement.executeQuery();
        List<Listener> listeners = new ArrayList<>();
        while (resultSet.next()) {
            String username = resultSet.getString("username");
            listeners.add(new Listener(participantRepository.findOne(username)));
        }
        return listeners;
    }
}
