package core.model;

import lombok.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 07.05.2017.
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Participant {
    private String username;
    private String password;
    private String name;
    private String affiliation;
    private String email;
    private String webPage;

    public Participant(ResultSet resultSet) throws SQLException {
        this(resultSet.getString("username"),
                resultSet.getString("password"),
                resultSet.getString("name"),
                resultSet.getString("affiliation"),
                resultSet.getString("email"),
                resultSet.getString("web_page"));
    }
}

