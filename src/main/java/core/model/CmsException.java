package core.model;

/**
 * Created on 07.05.2017.
 */
public class CmsException extends RuntimeException {
    public CmsException() {
        super();
    }

    public CmsException(String message) {
        super(message);
    }

    public CmsException(String message, Throwable cause) {
        super(message, cause);
    }

    public CmsException(Throwable cause) {
        super(cause);
    }
}
