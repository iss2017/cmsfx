package core.model;

import lombok.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created on 07.05.2017.
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Conference {
    private Integer conference_id;
    private String name;
    private Date startDate;
    private Date endDate;
    private Date deadline;

    public Conference(ResultSet resultSet) throws SQLException {
        this(resultSet.getInt("conference_id"),
                resultSet.getString("name"),
                resultSet.getDate("startDate"),
                resultSet.getDate("endDate"),
                resultSet.getDate("deadline"));

    }
}
