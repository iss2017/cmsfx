package core.model;

import lombok.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 07.05.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString(exclude = {"paperFile", "abstractFile"})
public class Submission {
    private Integer submissionId;
    private String title;
    private Integer sessionId;
    private String keywords;
    private String abstractName;
    private byte[] abstractFile;
    private String paperName;
    private byte[] paperFile;

    public Submission(ResultSet resultSet) throws SQLException {
        this(resultSet.getInt("submission_id"),
                resultSet.getString("title"),
                resultSet.getInt("session_id"),
                resultSet.getString("keywords"),
                resultSet.getString("abstract_name"),
                null,
                resultSet.getString("paper_name"),
                null);
    }

    public Submission(String title, Integer sessionId, String keywords, String abstractName, byte[] abstractFile,
                      String paperName, byte[] paperFile) {
        this(null, title, sessionId, keywords, abstractName, abstractFile, paperName, paperFile);
    }
}

