package core.model;

import lombok.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 07.05.2017.
 */
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Reviewer extends Participant
{
    public Reviewer(Participant participant) {
        super(participant.getUsername(), participant.getPassword(), participant.getName(),
                participant.getAffiliation(), participant.getEmail(), participant.getWebPage());
    }

    public Reviewer(String username, String password, String name, String affiliation, String email, String webPage) {
        super(username, password, name, affiliation, email, webPage);
    }

    public Reviewer(ResultSet resultSet) throws SQLException {
        super(resultSet);
    }

    @Override
    public String toString() {
        return "Reviewer + " + super.toString();
    }
}
