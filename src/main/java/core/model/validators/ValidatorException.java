package core.model.validators;

/**
 * Created on 07.05.2017.
 */
public class ValidatorException extends RuntimeException {
    public ValidatorException() {
        super();
    }

    public ValidatorException(String message) {
        super(message);
    }

    public ValidatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidatorException(Throwable cause) {
        super(cause);
    }
}
