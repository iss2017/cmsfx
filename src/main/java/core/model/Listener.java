package core.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 07.05.2017.
 */

public class Listener extends Participant {
    public Listener(Participant participant) {
        super(participant.getUsername(), participant.getPassword(), participant.getName(),
                participant.getAffiliation(), participant.getEmail(), participant.getWebPage());
    }

    public Listener(String username, String password, String name, String affiliation, String email, String webPage) {
        super(username, password, name, affiliation, email, webPage);
    }

    public Listener(ResultSet resultSet) throws SQLException {
        super(resultSet);
    }

    @Override
    public String toString() {
        return "Listener + " + super.toString();
    }
}
