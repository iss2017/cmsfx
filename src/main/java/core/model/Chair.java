package core.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 07.05.2017.
 */

public class Chair extends Participant {
    public Chair(Participant participant) {
        super(participant.getUsername(), participant.getPassword(), participant.getName(),
                participant.getAffiliation(), participant.getEmail(), participant.getWebPage());
    }

    public Chair(String username, String password, String name, String affiliation, String email, String webPage) {
        super(username, password, name, affiliation, email, webPage);
    }

    public Chair(ResultSet resultSet) throws SQLException {
        super(resultSet);
    }

    @Override
    public String toString() {
        return "Chair + " + super.toString();
    }
}
