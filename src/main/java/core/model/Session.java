package core.model;

import lombok.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 07.05.2017.
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Session {
    private Integer session_id;
    private String type;
    private Integer conferenceId;
    private Integer chairId;

    public Session(ResultSet resultSet) throws SQLException {
        this(resultSet.getInt("session_id"),
                resultSet.getString("type"),
                resultSet.getInt("conferenceId"),
                resultSet.getInt("chairId"));
    }
}
