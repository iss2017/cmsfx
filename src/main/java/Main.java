import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.sql.*;

/**
 * Created on 13.05.2017.
 */

public class Main extends Application {

    public static void main(String[] args) throws ClassNotFoundException {
        String url = "jdbc:postgresql://localhost:5432/cms",
                role = "postgres",
                password = "Default1";
        try (Connection connection = DriverManager.getConnection(url, role, password)) {
            createTables(connection);
            testRepo(connection);
            launch(args);
        } catch (SQLException e) {
            System.out.println("SQLException occurred!");
            System.out.println(e.getMessage());
        }
    }

    private static void testRepo(Connection connection) throws SQLException {
        //Test your repositories here
    }

    private static void createTables(Connection connection) throws SQLException {
        DatabaseMetaData dbm = connection.getMetaData();

        ResultSet participants = dbm.getTables(null, null, "participants", null);
        if (!participants.next()) {
            //Creates table "participants" only if it does not exist in the database
            String createParticipant = "CREATE TABLE participants " +
                    "(username VARCHAR(255) NOT NULL, " +
                    " password VARCHAR(255) NOT NULL, " +
                    " name VARCHAR(255) NOT NULL, " +
                    " affiliation VARCHAR(255) NOT NULL, " +
                    " email VARCHAR(255) NOT NULL, " +
                    " web_page VARCHAR(255), " +
                    " PRIMARY KEY ( username ))";
            Statement statement = connection.createStatement();
            statement.executeUpdate(createParticipant);
        }

        ResultSet conference = dbm.getTables(null, null, "conference", null);
        if (!conference.next()) {
            String createConference = "CREATE TABLE conference " +
                    "(conference_id SERIAL, " +
                    " name VARCHAR(255) NOT NULL, " +
                    " startDate DATE NOT NULL, " +
                    " endDate DATE , " +
                    " deadline DATE , " +
                    " PRIMARY KEY ( conference_id ))";
            Statement statement = connection.createStatement();
            statement.executeUpdate(createConference);
        }

        ResultSet chairs = dbm.getTables(null, null, "chairs", null);
        if (!chairs.next()) {
            String createChairs = "CREATE TABLE chairs " +
                    "(username VARCHAR(255) NOT NULL, " +
                    " FOREIGN KEY (username) REFERENCES participants(username), " +
                    " PRIMARY KEY ( username ))";
            Statement statement = connection.createStatement();
            statement.executeUpdate(createChairs);
        }

        ResultSet session = dbm.getTables(null, null, "session", null);
        if (!session.next()) {
            String createSession = "CREATE TABLE session " +
                    "(session_id SERIAL, " +
                    "type VARCHAR(255) NOT NULL, " +
                    " conference_id int NOT NULL, " +
                    " chair_username VARCHAR(255) REFERENCES chairs(username), " +
                    " FOREIGN KEY (conference_id) REFERENCES conference(conference_id), " +
                    " PRIMARY KEY ( session_id ))";
            Statement statement = connection.createStatement();
            statement.executeUpdate(createSession);
        }

        ResultSet submissions = dbm.getTables(null, null, "submissions", null);
        if (!submissions.next()) {
            String createSubmissions = "CREATE TABLE submissions " +
                    "(submission_id SERIAL, " +
                    " title VARCHAR(255) NOT NULL, " +
                    " session_id INT REFERENCES session(session_id), "  +
                    " keywords VARCHAR(255) NOT NULL, " +
                    " abstract_name VARCHAR(255), " +
                    " abstract_file BYTEA, " +
                    " paper_name VARCHAR(255), " +
                    " paper_file BYTEA, " +
                    " PRIMARY KEY ( submission_id ))";
            Statement statement = connection.createStatement();
            statement.executeUpdate(createSubmissions);
        }

        ResultSet listeners = dbm.getTables(null, null, "listeners", null);
        if (!listeners.next()) {
            String createListeners = "CREATE TABLE listeners " +
                    "(username VARCHAR(255) NOT NULL, " +
                    " FOREIGN KEY (username) REFERENCES participants(username), " +
                    " PRIMARY KEY ( username ))";
            Statement statement = connection.createStatement();
            statement.executeUpdate(createListeners);
        }

        ResultSet reviewers = dbm.getTables(null, null, "reviewers", null);
        if (!reviewers.next()) {
            String createReviewer = "CREATE TABLE reviewers " +
                    "(username VARCHAR(255) NOT NULL, " +
                    " PRIMARY KEY ( username )," +
                    " CONSTRAINT FK_ReviewersParticipants FOREIGN KEY (username) REFERENCES participants(username) )";
            Statement statement = connection.createStatement();
            statement.executeUpdate(createReviewer);
        }

        ResultSet reviewers_conferences = dbm.getTables(null, null, "reviewers_conferences", null);
        if (!reviewers_conferences.next()) {
            String createReviewersConferences = "CREATE TABLE reviewers_conferences " +
                    " (username VARCHAR(255) NOT NULL REFERENCES reviewers(username), " +
                    " conference_id INT NOT NULL REFERENCES conference(conference_id), " +
                    " PRIMARY KEY ( username, conference_id ))";
            Statement statement = connection.createStatement();
            statement.executeUpdate(createReviewersConferences);
        }
        //ADD TABLE CREATION HERE
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World!");
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(event -> System.out.println("Hello World!"));

        StackPane root = new StackPane();
        root.getChildren().add(btn);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }
}
